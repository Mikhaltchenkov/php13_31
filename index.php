<?php

/*
 * 1.
 *
 * Инкапсуляция - это сокрытие реализации класса и предохранение целостности его данных. Позволяет
 * использовать готовые классы при написании программ без знания и понимания их внутреннего устройства, имея лишь публичные
 * методы и поля в качестве интерфейса взаимодействия. А так же позволяет защитить данные объекта от
 * некорректного использования, за счет объявления полей приватными или защищенными и создания публичных методов для
 * оперирования этими полями (геттеры, сеттеры, прочие методы).
 *
 * 2.
 *
 * Плюсы:
 * + Объекты позволяют описать предметную область программы более естественным, понятным человеку образом;
 * + Объекты объединяют в себе все данные и операции над этими данными, что позволяет поддерживать некий
 * порядок в именовании в коде;
 * + При работе с объектами проще избежать ошибок в названиях, т.к. IDE среда подсказывает при вводе названия правильные;
 * + При разбиении предметной области на мелкие задачи - объекты, в последствии проще сопровождать и коллективно
 * разрабатывать проргамму;
 * Минусы:
 * - Для грамотного описания объектов нужно иметь достаточно знаний и опыта, чтоб в последствии ими было удобно пользоваться;
 * - Увеличение и усложнение кода при описании объектов. Некоторые программы проще написать в функциональном стиле, нежели в ООП.
 * - Уменьшение производительности кода при работе с ООП.
 */

class Car
{
    protected $name;
    protected $color;
    protected $speed;

    public function __construct($name, $color, $speed)
    {
        $this->name = $name;
        $this->color = $color;
        $this->speed = $speed;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    public function go()
    {
        echo $this->name . ' поехал';
    }

    public function turnLeft()
    {
        echo $this->name . ' повернул влево';
    }

    public function turnRigth()
    {
        echo $this->name . ' повернул направо';
    }
}

$lada = new Car('Lada 2105', 'Белый', 90);
$kia = new Car('KIA Rio', 'Синий', 190);

class Television
{
    protected $model;
    protected $displayType;
    protected $diagonal;
    protected $state;

    public function __construct($model, $channels, $displayType)
    {
        $this->model = $model;
        $this->diagonal = $channels;
        $this->displayType = $displayType;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getDiagonal()
    {
        return $this->diagonal;
    }

    public function setDiagonal($diagonal)
    {
        $this->diagonal = $diagonal;
    }

    public function getDisplayType()
    {
        return $this->displayType;
    }

    public function setDisplayType($displayType)
    {
        $this->displayType = $displayType;
    }

    public function turnOn($on)
    {
        $this->state = $on;
        echo $this->model . ' ' . $on ? 'ВКЛ' : 'ВЫКЛ';
    }

    public function getState()
    {
        return $this->state;
    }
}

$lg = new Television('LG 4260', 42, 'TFT');
$samsung = new Television('SAMSUNG T5276', 52, 'IPS');

class Duck
{
    protected $name;
    protected $color;

    public function __construct($name, $color)
    {
        $this->name = $name;
        $this->color = $color;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function fly()
    {
        echo $this->name . ' летит';
    }

    public function go()
    {
        echo $this->name . ' идет';
    }

    public function swim()
    {
        echo $this->name . ' плывет';
    }
}

$duck1 = new Duck('Donald', 'White');
$duck2 = new Duck('Duffy', 'Black');


class BallPen
{
    protected $color;
    protected $thin;

    public function __construct($color, $thin)
    {
        $this->color = $color;
        $this->thin = $thin;
    }

    public function write()
    {
        echo 'Ручка пишет, цвет ' . $this->color . ', толщина ' . $this->thin;
    }
}

$red = new BallPen('Красный', 0.5);
$blue = new BallPen('Синий', 0.3);

class Product
{
    protected $title;
    protected $price;
    protected $discount;

    public function __construct($title, $price, $discount)
    {
        $this->title = $title;
        $this->price = $price;
        $this->discount = $discount;
    }

    public function getPrice()
    {
        return $this->price - $this->price / 100 * $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

}

$product1 = new Product('Книга "Мастер и Маргарита"', 100, 5);
$product2 = new Product('Книга "Освой PHP7"', 400, 3);
